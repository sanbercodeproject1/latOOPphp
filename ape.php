<?php
  require_once("animal.php");

  class ape extends animal {
    public $name;
    public $cold_blooded = "No";
    public $legs = 2;
    public function __construct($nama) {
      $this->name = $nama;
    }
    public function yell() {
      echo "Auooo";
    }
  }
?>