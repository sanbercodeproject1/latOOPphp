<?php
  require_once("animal.php");

  class frog extends animal {
    public $name;
    public $cold_blooded = "Yes";
    public $legs = 2;
    public function __construct($nama) {
      $this->name = $nama;
    }
    public function jump() {
      echo "hop hop";
    }
  }
?>