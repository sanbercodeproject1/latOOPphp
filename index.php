<?php
  require_once("animal.php");
  require_once("frog.php");
  require_once("ape.php");

  $sheep = new animal("shaun");

  echo "Animal 1 : <br>" . "Animal name : " . $sheep->name . "<br>"; // "shaun"
  echo "Legs : " . $sheep->legs . "<br>"; // 4
  echo "Cold blooded type : ". $sheep->cold_blooded . "<br> <br>"; // "no"

  $kodok = new frog("buduk");

  echo "Animal 2 : <br>" . "Animal name : " . $kodok->name . "<br> Jump : " ; // "shaun"
  echo $kodok->jump() . "<br>"; // "hop hop"
  echo "Legs : " . $kodok->legs . "<br>"; // 2
  echo "Cold blooded type : " . $kodok->cold_blooded . "<br> <br>"; // "yes"

  $kera = new ape("kera sakti");

  echo "Animal 3 : <br>" . "Animal name : " . $kera->name . "<br> Yell : "; // "shaun"
  echo $kera->yell() . "<br>"; // "Auooo"
  echo "Legs : " . $kera->legs . "<br>"; // 2
  echo "Cold blooded type : " . $kera->cold_blooded . "<br> <br>"; // "yes"
?>